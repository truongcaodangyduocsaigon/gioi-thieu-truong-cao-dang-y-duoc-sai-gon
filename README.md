## GIỚI THIỆU TRƯỜNG CAO ĐẲNG Y DƯỢC SÀI GÒN ##

 **Hơn 10 năm thành lập và phát triển, Trường Cao đẳng Y Dược Sài Gòn luôn là địa chỉ đáng tin cậy để các bạn sinh viên có thể phát huy hết tiềm năng của bản thân bởi vì nhà trường luôn có mục tiêu đào tạo rõ ràng, cụ thể như sau:** 

![Trường Cao đẳng Y Dược Sài Gòn](https://caodangyduochochiminh.vn/images/files/caodangyduochochiminh.vn/hoat-dong-cua-truong-cao-dang-y-duoc-sai-gon-2.jpg)

• Đào tạo ra được những nguồn nhân lực có phẩm chất đạo đức; chính trị tốt, kiến thức và năng lực phù hợp để đáp ứng được những yêu cầu công việc đặt ra về sau;

• Lấy sinh viên làm trọng tâm và đẩy mạnh công tác đào tạo chuẩn theo như những quy định của Bộ Lao động TB&XH đã ra;

• Hàng năm phải cung cấp cho ngành Y tế những nguồn nhân lực chất lượng tốt;

• Đẩy mạnh phát triển thêm những chương trình nghiên cứu khoa học cấp Trường/ cấp Nhà nước cho sinh viên hay những cán bộ trong nhà trường được tham gia;

• Nhà trường sẽ tiến hành lên chiến lược và kế hoạch phát triển của trường theo từng thời kỳ/ từng năm cho phù hợp;

• Triển khai công tác tổ chức bộ máy quản lý, xây dựng và bồi dưỡng đội ngũ giảng viên trong hệ thống nhà trường. Đáp ứng đủ đội ngũ giảng viên về cả chất lượng và số lượng để phục vụ cho quá trình học tập cho sinh viên;

• Thực hiện những nhiệm vụ khác theo đúng quy định của Bộ Lao động TB & XH và Bộ Y tế đề ra theo từng năm.

**Với những nhiệm vụ đào tạo ấy, Cao đẳng Y Dược Sài Gòn là hệ Cao đẳng, đào tạo chuyên ngành Y Dược tại Sài Gòn, trong thời gian 3 năm, theo hình thức tích lũy tín chỉ, sau khi tốt nghiệp được cấp bằng Cao đẳng chính quy với ngành đào tạo:**

![Cách ngành đào tạo của Trường Cao đẳng Y Dược Sài Gòn](https://caodangyduochcm.vn/images/files/caodangyduochcm.vn/Y%20D%C6%B0%E1%BB%A3c%202018/cao-dang-duoc-tphcm-lay-bao-nhieu-diem-1.jpg)
 
1.  Cao đẳng Dược. Mã ngành: 6720201
 
2.  Cao đẳng Điều dưỡng. Mã ngành 6720301
 
3.  Cao đẳng Kỹ thuật Xét nghiệm Y học. Mã ngành: 6720602

4.  Cao đẳng Hộ sinh. Mã ngành: 6720303

5.  Cao đẳng Kỹ thuật Phục hồi chức năng. Mã ngành: 6720603

 
Kết, để biết thêm thông tin chi tiết về chương trình đào tạo **[Cao đẳng Dược ở Sài Gòn](https://caodangyduochcm.vn/)** cũng như các thông tin tuyển sinh của Cao đẳng Y Dược Sài Gòn, vui lòng tham khảo tại: 
 
- Website: caodangyduochcm.vn
- Email: Cdyduocsaigon@gmail.com 
- Hotline: 0287.1060.222 
- SĐT: 096.152.9898 - 093.851.9898 

- Địa chỉ: 

Cơ sở 1: Số 1036 Đường Tân Kỳ Tân Quý Tổ 129, Khu phố 14, Phường: Bình Hưng Hòa, Quận: Bình Tân, TP.HCM 

Cơ sở 2: Toà nhà : PTT - Đường số 3- Lô số 07, Công viên phần mềm Quang Trung, Phường: Tân Chánh Hiệp, Quận: 12, TP.HCM 

- Google Maps: [https://goo.gl/maps/qU2Nt1M1aXA2](https://goo.gl/maps/qU2Nt1M1aXA2)
